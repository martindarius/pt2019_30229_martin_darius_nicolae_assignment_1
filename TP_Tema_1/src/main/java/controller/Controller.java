package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import model.*;
import view.*;

public class Controller {
	private Polinom m_model;
	private Gui m_view;

	public Controller(Polinom model, Gui view) {
		m_model = model;
		m_view = view;

		view.butonDerivare(new butonDerivareListener());
		view.butonIntegrare(new butonIntegrareListener());
		view.butonExecuta(new butonExecutaListener());
		// view.operatiiBox(new operatiiBoxListener());
	}

	class butonDerivareListener implements ActionListener {

		public void actionPerformed(ActionEvent arg0) {
			String text = m_view.getText1();
			System.out.println(text);
			try {
				Polinom a = new Polinom(text);
				a = a.derivare();
				m_view.setTextOutput(a.toString());
			} catch (NumberFormatException e) {
				m_view.setTextOutput("Imput invalid!");
				m_view.showException("Imput invalid");
			}
		}

	}

	class butonIntegrareListener implements ActionListener {

		public void actionPerformed(ActionEvent arg0) {
			String text = m_view.getText1();
			System.out.println(text);
			try {
				Polinom a = new Polinom(text);
				a = a.integrare();
				m_view.setTextOutput(a.toString());
			} catch (NumberFormatException e) {
				m_view.setTextOutput("Imput invalid!");
				m_view.showException("Imput invalid");
			}
		}

	}

	class butonExecutaListener implements ActionListener {

		public void actionPerformed(ActionEvent arg0) {
			String text1 = m_view.getText1();
			String text2 = m_view.getText2();

			try {
				Polinom a = new Polinom(text1);
				Polinom b = new Polinom(text2);

				Polinom rez = new Polinom();

				String operatie = m_view.getOperatie();
				if (operatie.equals("Adunare")) {
					rez = a.adunare(b);
				}
				if (operatie.equals("Scadere")) {
					rez = a.scadere(b);
				}
				if (operatie.equals("Inmultire")) {
					rez = a.inmultire(b);
				}
				if (operatie.equals("Impartire")) {
					List<Polinom> rezImpartire = new ArrayList<Polinom>();
					rezImpartire = a.impartire(b);
					try {
						if (b.toString() == "0")
							throw new Exception();
						String outputImpartire = "";
						outputImpartire = "cat: " + rezImpartire.get(0).toString() + " rest: "
								+ rezImpartire.get(1).toString();

						m_view.setTextOutput(outputImpartire);
					} catch (Exception e) {
						m_view.showException("Divide by 0");
					}

				}

				if (operatie.equals("Impartire") == false) {

					m_view.setTextOutput(rez.toString());
				}
			} catch (NumberFormatException e) {
				m_view.setTextOutput("Imput invalid!");
				m_view.showException("Imput invalid");
			}
		}

	}
}
