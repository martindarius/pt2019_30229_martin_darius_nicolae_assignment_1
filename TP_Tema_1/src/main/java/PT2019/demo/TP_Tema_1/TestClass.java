package PT2019.demo.TP_Tema_1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import model.Polinom;

public class TestClass {

	@Test
	public void testAdunare() {
		Polinom a=new Polinom("3x+2");
		Polinom b=new Polinom("2x^2-5");
		a=a.adunare(b);
		assertEquals(a.toString(),"+2.0x^2+3.0x-3.0");
		assertNotEquals(a.toString(),"+2.0x^2+3.0x-3.0+1");
	}
	
	@Test
	public void testScadere() {
		Polinom a=new Polinom("3x+2-5x^2+9x^3");
		Polinom b=new Polinom("2x^2+3x-4");
		a=a.scadere(b);
		assertEquals(a.toString(),"+9.0x^3-7.0x^2+6.0");
		assertNotEquals(a.toString(),"+9.0x^3-7.0x^2+5.0");
	}
	
	@Test
	public void testInmultire() {
		Polinom a=new Polinom("x+7-23x^2");
		Polinom b=new Polinom("11x^3-9x");
		a=a.inmultire(b);
		assertEquals(a.toString(),"-253.0x^5+11.0x^4+284.0x^3-9.0x^2-63.0x");
		assertNotEquals(a.toString(),"-253.0x^4+11.0x^4+284.0x^3-9.0x^2-63.0x");
	}
	
	@Test
	public void testImpartire() {
		Polinom a=new Polinom("11x^4-3x^2+9x");
		Polinom b=new Polinom("10x^2+7x");
		List<Polinom> rezImpartire = new ArrayList<Polinom>();
		rezImpartire = a.impartire(b);
		assertEquals(rezImpartire.get(0).toString(),"+1.1x^2-0.77x+0.24");
		assertEquals(rezImpartire.get(1).toString(),"+7.32x");
		
		assertNotEquals(rezImpartire.get(0).toString(),"+1.1x^1-0.77x+0.24");
		assertNotEquals(rezImpartire.get(1).toString(),"+7.3x");
		
	}
	
	@Test
	public void testDerivare() {
		Polinom a=new Polinom("9x^7+2x^5-3x^3+2x-1");
		a=a.derivare();
		assertEquals(a.toString(),"+63.0x^6+10.0x^4-9.0x^2+2.0");
		assertNotEquals(a.toString(),"+63.0x^6+10.0x^4-9.0x^2+3.0");
	}
	
	@Test
	public void testIntegrare() {
		Polinom a=new Polinom("12x+7x^2-3x^3+8x^4+15x^6");
		a=a.integrare();
		assertEquals(a.toString(),"+2.14x^7+1.6x^5-0.75x^4+2.33x^3+6.0x^2");
		assertNotEquals(a.toString(),"+2.13x^7+1.6x^5-0.75x^4+2.33x^3+6.0x^2");
	}
	
	
	
	
}
