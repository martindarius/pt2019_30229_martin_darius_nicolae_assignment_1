package view;

import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Gui extends JFrame {
	String[] optiuni = { "Adunare", "Scadere", "Inmultire", "Impartire" };
	private JComboBox operatie = new JComboBox(optiuni);
	private JLabel text1 = new JLabel("Introduceti primul polinom:");
	private JLabel text2 = new JLabel("Introduceti al doilea polinom:");
	private JTextField input1 = new JTextField(40);
	private JTextField input2 = new JTextField(40);
	private JButton buton1 = new JButton("Derivare");
	private JButton buton2 = new JButton("Integrare");
	private JButton buton3 = new JButton("Executa");
	private JLabel text3 = new JLabel("Rezultat:");
	private JTextField output = new JTextField(40);
    private JLabel imagine=new JLabel(new ImageIcon("fundal.jpg"));
	public Gui() {
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(650, 350);
		this.setTitle("Calculator polinoame");
		JPanel c1 = new JPanel();
		JPanel c2 = new JPanel();
		JPanel c3 = new JPanel();
		JPanel c4 = new JPanel();
		JPanel c5 = new JPanel();
		JPanel c6 = new JPanel();
		JPanel c7 = new JPanel();
		JPanel c8 = new JPanel();
		JPanel c9 = new JPanel();
		

		c1.add(text1);
		c1.add(input1);
		c1.setLayout(new FlowLayout());
		c2.add(buton1);
		c2.add(buton2);
		c2.setLayout(new FlowLayout());
		c3.add(c1);
		c3.add(c2);
		c3.setLayout(new BoxLayout(c3, BoxLayout.Y_AXIS));
		c4.add(text2);
		c4.add(input2);
		c4.setLayout(new FlowLayout());
		c5.add(operatie);
		c5.add(buton3);
		c5.setLayout(new FlowLayout());
		c6.add(text3);
		c6.add(output);
		c6.setLayout(new FlowLayout());
	    imagine.add(c3);
		imagine.add(c4);
		imagine.add(c5);
		imagine.add(c6);
		imagine.setLayout(new FlowLayout());
		c7.add(imagine);
		c7.setLayout(new BoxLayout(c7, BoxLayout.Y_AXIS));
        
        
        
		this.setContentPane(c7);
		this.setVisible(true);

	}

	public void butonDerivare(ActionListener a) {
		buton1.addActionListener(a);
	}

	public void butonIntegrare(ActionListener a) {
		buton2.addActionListener(a);
	}

	public void butonExecuta(ActionListener a) {
		buton3.addActionListener(a);
	}

	public void operatiiBox(ActionListener a) {
		operatie.addActionListener(a);
	}

	public String getText1() {
		return input1.getText();
	}

	public String getText2() {
		return input2.getText();
	}

	public void setTextOutput(String text) {
		output.setText(text);
	}

	public String getOperatie() {
		return operatie.getSelectedItem().toString();
	}

	public void showException(String mesaj) {
		JOptionPane.showMessageDialog(this,mesaj);
	}
}
