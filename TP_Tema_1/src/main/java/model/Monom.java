package model;

public class Monom implements Comparable<Monom> {
	private float coeficient;
	private int putere;

	public Monom(float coeficient, int putere) {
		// super();
		this.coeficient = coeficient;
		this.putere = putere;
	}

	public float getCoeficient() {
		return (float) (Math.round(coeficient * 100) / 100.0);
	}

	public void setCoeficient(float coeficient) {
		this.coeficient = coeficient;
	}

	public int getPutere() {
		return putere;
	}

	public void setPutere(int putere) {
		this.putere = putere;
	}

	public int compareTo(Monom a) {
		if (((Monom) a).getPutere() > this.getPutere())
			return 1;
		else if (((Monom) a).getPutere() < this.getPutere())
			return -1;
		else
			return 0;
	}

	public Monom impartire(Monom a) {
		Monom c = new Monom(0, 0);
		c.setCoeficient(this.getCoeficient() / a.getCoeficient());
		c.setPutere(this.getPutere() - a.getPutere());
		return c;
	}

}
