package model;

import java.util.*;

public class Polinom {
	private List<Monom> lista = new ArrayList<Monom>();

	public Polinom(String text) {
		this.parsare(text);
	}

	public Polinom() {
		Monom a = new Monom(0, 0);
		lista.add(a);
	}

	public List<Monom> getLista() {
		return lista;
	}

	public void setLista(List<Monom> lista) {
		this.lista = lista;
	}

	public void parsare(String text) {
		text = text.replaceAll(" ", "");
		System.out.println(text);
		this.verificaImput(text);
		if (text.charAt(0) != '-') {
			text = "+" + text;
		}
		String[] parts = text.split("(?=[+-])");

		for (String s : parts) {
			s = s.replace("+x", "+1x");
			s = s.replace("-x", "-1x");
			if (s.charAt(s.length() - 1) == 'x') {
				s = s.replace("x", "x^1");
			}
			if (s.charAt(s.length() - 1) > '0' && (s.charAt(s.length() - 1) < '9') && (s.indexOf('x') == -1)) {
				s = s + "x^0";
			}
			System.out.println(s);
			String[] parts2 = s.split("x\\^");
			float coef;
			int putere;
			if (parts2.length == 1) {
				coef = Float.parseFloat(parts2[0]);
				putere = 0;
			} else {
				coef = Float.parseFloat(parts2[0]);
				putere = Integer.parseInt(parts2[1]);
			}
			Monom x = new Monom(coef, putere);
			lista.add(x);
		}
		this.restrangePolinom();
		Collections.sort(lista);
	}

	public void restrangePolinom() {
		Polinom x = new Polinom();
		for (Monom i : this.lista) {
			boolean exista = false;
			for (Monom j : x.lista) {
				if (i.getPutere() == j.getPutere()) {
					exista = true;
					j.setCoeficient(j.getCoeficient() + i.getCoeficient());
				}
			}
			if (exista == false) {
				x.lista.add(i);
			}
		}
		this.lista = x.lista;
	}

	public Polinom adunare(Polinom b) {

		for (Monom i : b.lista) {
			boolean exista = false;
			for (Monom j : this.lista) {
				if (i.getPutere() == j.getPutere()) {
					exista = true;
					j.setCoeficient(i.getCoeficient() + j.getCoeficient());
				}

			}
			if (exista == false) {
				this.lista.add(i);
			}
		}
		Collections.sort(lista);
		return this;
	}

	public Polinom scadere(Polinom b) {
		for (Monom i : b.lista) {
			boolean exista = false;
			for (Monom j : this.lista) {
				if (i.getPutere() == j.getPutere()) {
					exista = true;
					j.setCoeficient(j.getCoeficient() - i.getCoeficient());

				}
			}
			if (exista == false) {
				float x = i.getCoeficient();
				x = -x;
				i.setCoeficient(x);
				this.lista.add(i);
			}
		}
		Collections.sort(lista);
		return this;
	}

	public Polinom inmultire(Polinom b) {
		Polinom c = new Polinom();
		for (Monom i : this.lista) {
			for (Monom j : b.lista) {
				Monom x = new Monom(1, 1);
				x.setCoeficient(i.getCoeficient() * j.getCoeficient());
				x.setPutere(i.getPutere() + j.getPutere());
				boolean exista = false;
				for (Monom z : c.lista) {
					if (z.getPutere() == x.getPutere()) {
						exista = true;
						z.setCoeficient(x.getCoeficient() + z.getCoeficient());
					}
				}
				if (exista == false) {
					c.lista.add(x);
				}
			}
		}
		Collections.sort(c.lista);
		return c;
	}

	public List<Polinom> impartire(Polinom b) {
		List<Polinom> rez = new ArrayList<Polinom>();
		if (Polinom.isEmpty(b) == false) {
			Polinom q = new Polinom();
			Polinom r = new Polinom();
			r = this;
			while ((Polinom.isEmpty(r) == false) && (Polinom.gradMax(r) >= Polinom.gradMax(b))) {
				Monom t = new Monom(0, 0);
				int i = 0;
				while (r.getLista().get(i).getCoeficient() == 0) {
					i++;
				}
				t = r.getLista().get(i).impartire(b.getLista().get(0));
				q.lista.add(t);
				Polinom b2 = new Polinom();
				b2 = b2.copiere(b);
				b2.inmultirePolinomMonom(t);
				r.scadere(b2);
			}
			rez.add(q);
			rez.add(r);
		}

		return rez;
	}

	public Polinom derivare() {
		for (Monom i : this.lista) {
			i.setCoeficient(i.getCoeficient() * i.getPutere());
			i.setPutere(i.getPutere() - 1);
		}
		return this;
	}

	public Polinom integrare() {
		for (Monom i : this.lista) {
			i.setPutere(i.getPutere() + 1);
			i.setCoeficient(i.getCoeficient() / i.getPutere());
		}
		return this;
	}

	public String toString() {
		String s = "";
		for (Monom i : this.lista) {
			if (i.getCoeficient() != 0 && i.getPutere() != -1) {
				if (i.getCoeficient() > 0) {
					if (i.getPutere() == 0) {
						s = s + "+" + i.getCoeficient();
					} else if (i.getPutere() == 1) {
						s = s + "+" + i.getCoeficient() + "x";
					} else {
						s = s + "+" + i.getCoeficient() + "x^" + i.getPutere();
					}
				} else if (i.getCoeficient() < 0) {
					if (i.getPutere() == 0) {
						s = s + i.getCoeficient();
					} else if (i.getPutere() == 1) {
						s = s + i.getCoeficient() + "x";
					} else {
						s = s + i.getCoeficient() + "x^" + i.getPutere();
					}
				}
			}
		}
		if (s == "")
			s = "0";
		return s;
	}

	public static boolean isEmpty(Polinom a) {
		boolean empty = true;
		for (Monom x : a.lista) {
			if (x.getCoeficient() != 0) {
				empty = false;
			}
		}
		return empty;
	}

	public static int gradMax(Polinom a) {
		int i = 0;
		while (a.getLista().get(i).getCoeficient() < 0.1 && a.getLista().get(i).getCoeficient() > -0.1) {
			a.getLista().get(i).setCoeficient(0);
			i++;
		}
		return a.getLista().get(i).getPutere();
	}

	public Polinom inmultirePolinomMonom(Monom a) {

		for (Monom x : this.lista) {
			x.setCoeficient(x.getCoeficient() * a.getCoeficient());
			x.setPutere(x.getPutere() + a.getPutere());
		}
		return this;
	}

	public Polinom copiere(Polinom a) {
		Polinom rez = new Polinom();
		for (Monom i : a.lista) {
			Monom nou = new Monom(0, 0);
			nou.setCoeficient(i.getCoeficient());
			nou.setPutere(i.getPutere());
			rez.lista.add(nou);
		}
		return rez;
	}

	public void verificaImput(String text) throws NumberFormatException {
		String text2;
		text2 = text.substring(0, text.length());
		text2 = text2.replaceAll("x", "");
		text2 = text2.replaceAll("\\^", "");
		text2 = text2.replaceAll("\\+", "");
		text2 = text2.replaceAll("\\-", "");
		for (char x : text2.toCharArray()) {
			if (x < '0' || x > '9') {
				throw new NumberFormatException();
			}
		}
	}

}
